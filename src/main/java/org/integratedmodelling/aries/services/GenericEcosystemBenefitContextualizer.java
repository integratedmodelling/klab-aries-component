package org.integratedmodelling.aries.services;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Generic ES process contextualizer. Keeps track of all values flowing through the
 * flow network, builds snapshots and final statistics.
 * 
 * @author Ferd
 *
 */
@Prototype(
        id = "aries.es",
//        args = { "? d|distance-threshold", Prototype.FLOAT },
//        argDescriptions = {
//                "distance threshold in km (no connections created beyond this) - default 100km"
//        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class GenericEcosystemBenefitContextualizer implements IProcessContextualizer {

    @Override
    public boolean canDispose() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
            throws KlabValidationException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation contextSubject, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

}
