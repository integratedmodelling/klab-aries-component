/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.aries.services;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.aries.AriesComponent;
import org.integratedmodelling.aries.Flows;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.engine.geospace.gis.SextanteOperations;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.engine.modelling.runtime.Relationship;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;

@Prototype(
        id = "im.aries.aesthetic-flow",
        args = { "? d|distance-threshold", Prototype.FLOAT },
        argDescriptions = {
                "distance threshold in km (no connections created beyond this) - default 100km"
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class AestheticViewBenefitContextualizer implements IProcessContextualizer {

    IProject                 project;
    boolean                  canDispose;
    IScale                   scale;
    IMonitor                 monitor;
    IActiveDirectObservation context;
    IResolutionScope         resolutionContext;

    IState   elevation;
    IState   visualBlight;
    IConcept viewshedObservable;

    // SpatialDisplay debug;

    Map<ISubject, IState> viewsheds = new HashMap<>();
    private IProcess      process;

    /*
     * TODO set into a parameter
     */
    private double DISTANCE_THRESHOLD_KM = 100;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
        this.project = project;
    }

    /**
     * Get the viewshed of a particular transactor, computing it if necessary.
     * 
     * @param transactor
     * @return viewshed for subject
     * @throws KlabException
     */
    public IState getViewshed(ISubject transactor) throws KlabException {

        IState ret = viewsheds.get(transactor);
        if (ret == null) {
            /*
             * TODO this should become simply the resolution of Visibility in region according to transactor.
             * 
             */
            ObservableSemantics observable = new ObservableSemantics(ModelFactory
                    .presenceObserver(viewshedObservable), transactor, "visible-region");
            monitor.info("computing viewshed of " + transactor.getName(), Messages.INFOCLASS_MODEL);
            ret = SextanteOperations.getRasterViewshed(elevation, context, transactor, observable, monitor);
            viewsheds.put(transactor, ret);
        }
        return ret;
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        this.resolutionContext = resolutionContext;

        Map<String, IObservation> ret = new HashMap<>();

        IConcept visregion = Observables.declareObservable(GeoNS.PLAIN_REGION, Collections.singleton(GeoNS.VISIBLE_TRAIT));
        this.viewshedObservable = Observables.makePresence(visregion);

        this.context = context;
        this.process = process;
        this.scale = context.getScale();
        this.canDispose = !scale.isTemporallyDistributed();
        this.monitor = monitor;

        // debug = new SpatialDisplay((SpaceExtent) scale.getSpace());

        if (!this.scale.isSpatiallyDistributed() || this.scale.getSpace().getGrid() == null) {
            throw new KlabUnsupportedOperationException("the aesthetic benefit contextualizer only works on gridded contexts for now");
        }

        /*
         * find elevation and visual blight state if any.
         */
        this.elevation = States.findState(context, AriesComponent.NS.ELEVATION);
        this.visualBlight = States.findState(context, AriesComponent.NS.VISUAL_BLIGHT);

        for (IRelationship rel : buildFlowNetwork(context)) {
            ret.put(rel.getName(), rel);
        }

        /*
         * temporary - may want to associate to an option
         */
        for (ISubject s : viewsheds.keySet()) {
            ret.put(s.getName() + "_viewshed", s);
        }

        return ret;
    }

    private Iterable<IRelationship> buildFlowNetwork(IDirectObservation context) throws KlabException {

        ArrayList<IRelationship> ret = new ArrayList<>();

        int nc = 0;
        int nt = 0;
        for (ISubject provider : Flows.getProviders((ISubject) context, process)) {

            for (ISubject transactor : Flows.getTransactors((ISubject) context, process)) {
 
                nt++;
                
                IRelationship rel = buildVisualConnection(provider, transactor);
                if (rel != null) {
                    ((ISubject) context).getStructure().link(provider, transactor, rel);
                    ret.add(rel);
                    nc++;
                }
            }
        }

        if (nc == 0) {
            monitor.warn("no visual connections among " + nt + " possible: no aesthetic flows in the context");
        } else {
            monitor.info(nc + " visual connections made", Messages.INFOCLASS_MODEL);
        }

        /*
         * TODO use a generalized connector function to link beneficiaries to transactors.
         * At the moment we have no beneficiaries. The model used should be a
         * use-in-place, come-from-anywhere type.
         */

        // debug.show();

        return ret;
    }

    private IRelationship buildVisualConnection(ISubject provider, ISubject transactor)
            throws KlabException {

        /*
         * create or retrieve the viewshed for the transactor...
         */
        IState viewshed = getViewshed(transactor);
        /*
         * ...and the provider-specific piece of it
         */
        IState tviewshed = States.getView(viewshed, provider);

        /*
         * false = not a bit of provider in view of this transactor.
         */
        boolean inView = (Boolean) tviewshed.getValue(0);
        if (!inView) {
            return null;
        }

        double percentInView = 1.0;
        if (tviewshed.getMetadata().contains(IState.Mediator.SPACE_TOTAL_VALUES)) {
            percentInView = (double) tviewshed.getMetadata().getInt(IState.Mediator.SPACE_VALUE_SUM)
                    / (double) tviewshed.getMetadata().getInt(IState.Mediator.SPACE_TOTAL_VALUES);
        }

        /*
         * Visual connection's spatial context: polygon joining each viewpoint and the
         * whole feature.
         */
        ShapeValue connectionField = Flows
                .createJoiningShape(provider.getScale().getSpace().getShape(), transactor
                        .getScale().getSpace().getShape());

        // clip to overall context - there's a slight change that the convex hull
        // operation creates weird
        // things.
        connectionField = ((ShapeValue) scale.getSpace().getShape()).intersection(connectionField);

        // debug.add(connectionField, "connections");

        /*
         * intersect the connection with the viewshed and visual blight
         */

        IState cviewfield = States.getView(viewshed, Scale.substituteExtent(this.scale, connectionField
                .asExtent()));

        double opennessValue = 1.0; // (double) visib / (double) total;
        // this is obviously true but we need the side effects.
        cviewfield.getValue(0);
        if (cviewfield.getMetadata().contains(IState.Mediator.SPACE_TOTAL_VALUES)) {
            opennessValue = (double) cviewfield.getMetadata().getInt(IState.Mediator.SPACE_VALUE_SUM)
                    / (double) cviewfield.getMetadata().getInt(IState.Mediator.SPACE_TOTAL_VALUES);
        }

        /*
         * TODO visual blight
         */
        double totalBlight = Double.NaN;
        if (visualBlight != null) {
            IState ugly = States.getView(visualBlight, Scale.substituteExtent(this.scale, connectionField
                    .asExtent()));
            totalBlight = (double) ugly.getValue(0);
        }

        /*
         * value of provider depends on distance, relative area seen, total area, and
         * visual blight in between. TODO the way these are aggregated should be
         * configurable through an expression in parameters.
         */
        double providerValue = provider.getScale().getSpace().getShape().getArea();
        double distance = GeoNS
                .getDistance((IGeometricShape) provider.getScale().getSpace()
                        .getShape(), (IGeometricShape) transactor
                                .getScale()
                                .getSpace().getShape());

        if (distance > DISTANCE_THRESHOLD_KM) {
            return null;
        }

        monitor.info(transactor.getName() + " sees "
                + NumberFormat.getPercentInstance().format(percentInView) + " of " + provider.getName()
                + " in a "
                + NumberFormat.getPercentInstance().format(opennessValue)
                + " open field", Messages.INFOCLASS_MODEL);

        /*
         * create the relationship
         */
        String cname = provider.getName() + "->" + transactor.getName();
        IRelationship ret = ((IActiveSubject) context)
                .connect((IActiveSubject)provider, (IActiveSubject)transactor, AriesComponent.NS.VISUAL_CONNECTION, Scale
                        .substituteExtent(context.getScale(), connectionField
                                .asExtent()));
        ((DirectObservation)ret).setName(cname);

        /*
         * add states for all the info
         */
        IState dState = ((Relationship) ret)
                .getStaticState(new ObservableSemantics(ModelFactory
                        .measureObserver(NS.DISTANCE, "km"), null, "viewing-distance"));
        States.set(dState, distance, 0);
        // IState bState = ret
        // .getStaticState(new Observable(ModelFactory
        // .proportionObserver(AriesComponent.NS.VISUAL_BLIGHT), ret,
        // "viewing-distance"));
        // States.set(bState, distance, 0);

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {

        canDispose = transition.isLast();
        // TODO Auto-generated method stub
        return null;
    }

}
