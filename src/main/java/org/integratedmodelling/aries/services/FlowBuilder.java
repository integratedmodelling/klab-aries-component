package org.integratedmodelling.aries.services;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IRelationshipInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

@Prototype(
        id = "aries.flows",
        args = { "? d|distance-threshold", Prototype.FLOAT },
        argDescriptions = {
                "distance threshold in km (no connections created beyond this) - default 100km"
        },
        returnTypes = { NS.RELATIONSHIP_INSTANTIATOR })
public class FlowBuilder implements IRelationshipInstantiator {

    @Override
    public void initialize(IActiveSubject contextSubject, IResolutionScope context, IModel callingModel, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean canDispose() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
            throws KlabValidationException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Map<String, IObservation> createRelationships(IActiveDirectObservation context, ITransition transition, Map<String, IState> inputs)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

}
