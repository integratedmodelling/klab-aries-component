/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.aries.water;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.aries.AriesComponent;
import org.integratedmodelling.aries.Flows;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Traits;
import org.integratedmodelling.engine.modelling.runtime.Relationship;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;

import com.vividsolutions.jts.geom.Point;

@Prototype(
        id = "im.aries.water-supply",
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class WaterProvisionBenefitContextualizer implements IProcessContextualizer {

    IProject           project;
    boolean            canDispose;
    IScale             scale;
    IMonitor           monitor;
    IDirectObservation context;
    IState             runoff;
    IProcess           process;

    Map<ISubject, Point> intakes   = new HashMap<>();
    List<IRelationship>  provision = new ArrayList<>();

    private IGrid            grid;
    private IResolutionScope resolutionContext;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
        this.project = project;
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        this.resolutionContext = resolutionContext;

        Map<String, IObservation> ret = new HashMap<>();

        this.context = context;
        this.scale = context.getScale();
        this.canDispose = !scale.isTemporallyDistributed();
        this.monitor = monitor;
        this.process = process;

        if (!context.getObservable().getSemantics().is(AriesComponent.NS.WATERSHED)) {
            throw new KlabUnsupportedOperationException("the water supply process should be observed in a watershed as the main provider.");
        }

        if (!this.scale.isSpatiallyDistributed() || this.scale.getSpace().getGrid() == null) {
            throw new KlabUnsupportedOperationException("the water supply contextualizer only works on gridded contexts for now");
        }

        this.grid = this.scale.getSpace().getGrid();

        /*
         * find elevation and visual blight state if any.
         */
        this.runoff = States.findState(context, AriesComponent.NS.RUNOFF_VOLUME);

        for (IRelationship rel : buildFlowNetwork(context)) {
            ret.put(rel.getName(), rel);
        }

        return ret;
    }

    private Iterable<IRelationship> buildFlowNetwork(IDirectObservation context) throws KlabException {

        ArrayList<IRelationship> ret = new ArrayList<>();

        /*
         * link transactors. Beneficiaries later.
         */
        int nc = 0;
        for (ISubject transactor : Flows.getTransactors((ISubject) context, process)) {
            IRelationship rel = buildConnection((ISubject) context, transactor);
            ((ISubject) context).getStructure().link((ISubject) context, transactor, rel);
            ret.add(rel);
            this.provision.add(rel);
            nc++;
        }

        if (nc == 0) {
            monitor.warn("no connections: no water ES flows in the context");
        } else {
            monitor.info(nc + " connections made", Messages.INFOCLASS_MODEL);
        }

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {

        canDispose = transition.isLast();

        for (IRelationship rel : provision) {

            ISubject transactor = rel.getTarget();
            Point tloc = intakes.get(transactor);

            long ofs = scale.locate(new SpaceLocator(tloc.getX(), tloc.getY()), transition);
            double d = getDouble(runoff.getValue((int) ofs));
            if (d > 0) {
                /*
                 * TODO generate value according to what the transactor is.
                 */
                for (IState state : rel.getStates()) {
                    if (Traits.hasTrait(state.getObservable().getSemantics().getType(), AriesComponent.NS.DAILY_TRAIT)) {
                        // TODO this needs to use simple storage. It cannot be seen
                        // anyway, so screw it for
                        // now.
                        // States.set(state, d, transition.getTimeIndex());
                    } else {
                        double prev = States.getDouble(state, 0);
                        States.set(state, prev + d, 0);
                    }
                }
            }
        }

        return null;
    }

    private double getDouble(Object value) {
        return value instanceof Number ? ((Number) value).doubleValue() : Double.NaN;
    }

    private IRelationship buildConnection(ISubject provider, ISubject transactor)
            throws KlabException {

        /*
         * value of provider depends on distance, relative area seen, total area, and
         * visual blight in between. TODO the way these are aggregated should be
         * configurable through an expression in parameters.
         */
        double providerValue = provider.getScale().getSpace().getShape().getArea();

        IConcept connection = Traits
                .hasTrait(transactor.getObservable().getSemantics().getType(), AriesComponent.NS.RIVAL_TRAIT)
                        ? AriesComponent.NS.WATER_CONSUMPTION_CONNECTION
                        : AriesComponent.NS.WATER_NON_CONSUMPTION_CONNECTION;

        /*
         * create the relationship
         */
        String cname = provider.getName() + "->" + transactor.getName();
        Relationship ret = new Relationship(((ISubject) context)
                .getStructure(), new ObservableSemantics(connection), transactor
                        .getScale(), KLAB.MMANAGER.getLocalNamespace(), provider, transactor, cname, monitor);

        IConcept waterValueObservable = Traits
                .hasTrait(transactor.getObservable().getSemantics().getType(), AriesComponent.NS.RIVAL_TRAIT)
                        ? AriesComponent.NS.WATER_CONSUMED
                        : AriesComponent.NS.WATER_FLOWED;

        IConcept dailyFlowObservable = Observables.declareObservable(waterValueObservable, Collections
                .singleton(AriesComponent.NS.DAILY_TRAIT));

        IState accumulatedFlow = ret
                .getStaticState(new ObservableSemantics(ModelFactory
                        .measureObserver(waterValueObservable, "mm"), null, "accumulated-flow"));

        IState dailyFlow = ret
                .getState(new ObservableSemantics(ModelFactory
                        .measureObserver(dailyFlowObservable, "mm"), null, "daily-flow"));

        States.set(accumulatedFlow, 0.0, 0);
        States.set(dailyFlow, 0.0, 0);

        Point zio = ((IGeometricShape) transactor.getScale().getSpace()).getGeometry().getCentroid();

        // IState intake = States.getView(runoff, transactor.getScale());
        intakes.put(transactor, zio);

        return ret;
    }
}
