/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.aries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.KlabException;

import com.vividsolutions.jts.algorithm.ConvexHull;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

public class Flows {

    /**
     * Create the flow network by identifying all 
     * @return
     * @throws KlabException 
     */
    static public Collection<IObservation> createFlowNetwork(ISubject subject, IProcess process) throws KlabException {

        List<IObservation> ret = new ArrayList<>();

        /*
         * find each P and B
         * resolve them and connect them all
         */
        for (ISubject p : getProviders(subject, process)) {
            for (ISubject b : getBeneficiaries(subject, process)) {
                ret.addAll(connect(p, b, subject));
            }
        }

        return ret;
    }

    /*
     * resolve all relationships and return the ones that survive or are generated, with
     * any additional object.
     */
    static public Collection<IObservation> connect(ISubject provider, ISubject beneficiary, ISubject context) throws KlabException {

        List<IObservation> ret = new ArrayList<>();
        
        ((Subject)provider).initialize(((Subject)context).getResolutionScope(), null, ((Subject)context).getMonitor());
        
        ret.add(provider);
        ret.add(beneficiary);

        return ret;
    }

    static public Iterable<ISubject> getProviders(ISubject subject, IProcess process) {
        ArrayList<ISubject> ret = new ArrayList<>();
        for (ISubject s : subject.getSubjects()) {
            if (NS.contains(AriesComponent.NS.PROVIDER_TRAIT, process.getRolesFor(s))) {
                ret.add(s);
            }
        }
        return ret;
    }

    static public Iterable<ISubject> getTransactors(ISubject subject, IProcess process) {
        ArrayList<ISubject> ret = new ArrayList<>();
        for (ISubject s : subject.getSubjects()) {
            if (NS.contains(AriesComponent.NS.TRANSACTOR_TRAIT, process.getRolesFor(s))) {
                ret.add(s);
            }
        }
        return ret;
    }

    static public Iterable<ISubject> getBeneficiaries(ISubject subject, IProcess process) {
        ArrayList<ISubject> ret = new ArrayList<>();
        for (ISubject s : subject.getSubjects()) {
            if (NS.contains(AriesComponent.NS.BENEFICIARY_TRAIT, process
                    .getRolesFor(s))) {
                ret.add(s);
            }
        }
        return ret;
    }

    static public ShapeValue createJoiningShape(IShape shape, IShape point) {

        String srs = "EPSG:" + ((IGeometricShape) shape).getSRID();
        Geometry boundary = ((IGeometricShape) shape).getGeometry().getBoundary();
        Geometry pt = ((IGeometricShape) point).getGeometry().getCentroid();

        Coordinate[] cloud = new Coordinate[boundary.getCoordinates().length + 1];
        int i = 1;
        cloud[0] = pt.getCoordinate();
        for (Coordinate c : boundary.getCoordinates()) {
            cloud[i++] = c;
        }
        ConvexHull hull = new ConvexHull(cloud, boundary.getFactory());
        return new ShapeValue(hull.getConvexHull().buffer(0), Geospace.getCRSFromID(srs));
    }
}
